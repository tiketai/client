<?php
defined('TIKETAI_FLOW') OR exit('No direct script access allowed');

/*
* ========================================
*   CONFIGURATION
* ========================================
*   Para configurar tu propio punto de venta debes cambiar *MI SUBDOMINIO TIKET.AI* por tu subdominio en tiket.ai. Por ejemplo
*   si tu ticketera es miticketera.tiket.ai tu subdominio sería 'miticketera'.*
*/


$conf['business_subdomain']     = '*AQUÍ SUBDOMINIO TIKET.AI*';     // Default: --

$conf['index_page']             = '/';                              // Default: '/'
$conf['timezone']               = 'America/Santiago';               // Default: 'America/Santiago'
$conf['locale']                 = 'es_CL';                          // Default: 'es_CL'


/*
* ========================================
*   END CONFIGURATION
* ========================================
*/







date_default_timezone_set($conf['timezone']);
setlocale(LC_TIME, $conf['locale'] );
define('INDEX_PAGE' , $conf['index_page']);
ini_set("display_errors", 0);
ini_set("log_errors", 1);

$protocol = 'http';
$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
$base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
define('BASE_URL' , $base_url.INDEX_PAGE);

if ( ! is_tiketai() ) {

    define('BUSINESS_SUBDOMAIN' , $conf['business_subdomain']);
    define('EXTERNAL_WEB_POS' , true);

}
else {

    $domain_array = explode('.', $_SERVER['HTTP_HOST']);
    $subdomain = array_shift($domain_array);
    define('BUSINESS_SUBDOMAIN' , $subdomain );
    define('EXTERNAL_WEB_POS' , false);

}



function is_tiketai()
{
    $server_name_array =  explode('.', $_SERVER['SERVER_NAME']);

    if ( end($server_name_array) == 'ai' && prev($server_name_array) == 'tiket' ) {
        define('API_URL' , (prev($server_name_array) == 'dev')?  'https://api-dev.tiket.ai/api/':'https://api.tiket.ai/api/');
        return TRUE;
    }else {
        define('API_URL' , 'https://api.tiket.ai/api/');
        return FALSE;
    }
}


function shutdownHandler()
{
    $error = error_get_last();
    if ($error['type'] == E_ERROR) {
            header("Refresh:0");
        }
}
register_shutdown_function('shutdownHandler');
